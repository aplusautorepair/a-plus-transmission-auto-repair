A Plus Transmission & Auto Repair is a full service auto repair facility specializing in transmission and driveline repairs. Proudly providing complete automotive repair to Brighton, MI and the surrounding areas for over 41 years, we are fully equipped to handle all car repairs and maintenance.

Address: 4355 S Old US Hwy 23, Brighton, MI 48114, USA

Phone: 810-229-7878

Website: https://www.aplustransmission.com
